import socket
sk = socket.socket()
sk_res=sk.connect_ex(('localhost', 8080))

# 测试数据
d1=b'~\x07\x04\x00o\x01A\x19P\x10\x07\x01L\x00\x03\x01\x00"\x00\x00\x00\x00\x00\x00\x00\x03\x02\t!\x84\x06\xb3\xf3 \x00\x00\x00\x00\x00Z\x19\t\x16\t0\x07\x01\x04\x00\x00\x03W\x00"\x00\x00\x00\x00\x00\x00\x00\x03\x02\t!\x84\x06\xb3\xf3 \x00\x00\x00\x00\x00Z\x19\t\x16\t07\x01\x04\x00\x00\x03W\x00"\x00\x00\x00\x00\x00\x00\x00\x03\x02\t!\x84\x06\xb3\xf3 \x00\x00\x00\x00\x00Z\x19\t\x16\t1\x07\x01\x04\x00\x00\x03W\xd6~'
d2=b'~\x02\x00\x00"\x01A\x19P\x10\x01\x059\x00\x00\x00\x00\x00\x00\x00\x03\x02\t24\x06\xb4q\xd0\x00\x00\x00\x00\x01@\x19\t\x16\t\'\x07\x01\x04\x00\x00\x02]-~'
d3=b'~\x02\x00\x00"\x01A\x19P\x10\x01\x04\xcc\x00\x00\x00\x00\x00\x00\x00\x03\x02\t)\xfc\x06\xb4\x17\xe0\x00\x00\x00\x00\x00\x00\x19\t\x16\t\x00\x05\x01\x04\x00\x00\x02>[~'
d4=b'~\x01\x00\x00!\x019\x11\x13\x11\x19\x00\x15\x00,\x00!11111GS03A\x00\x00\x000000099\x01\xd4\xa5A12345]~'
d5=b'~\x00\x02\x00\x00\x010\x02w\x19\x95\x0c\x10\xd6~'
d6=b"~\x00\x02\x00\x00\x01@'\x86\t\x19\x0c\x10\xee~"
d7=b'~\x07\x04\x00\x81\x01A\x19P\x10p\x02]\x00\x03\x01\x00(\x00\x00\x00\x00\x00\x00\x00\x03\x02\x08\x93,\x06\xb3\x82\xc8\x00\x00\x00Z\x00\xe6 \t\x18\x16\x11"\x01\x04\x00\x01\x17\x8b+\x04(\x10\x1f\xf2\x00(\x00\x00\x00\x00\x00\x00\x00\x03\x02\x08\x92\x08\x06\xb3\x82\x08\x00\x00\x002\x00\xfa \t\x18\x16\x11R\x01\x04\x00\x01\x17\x8b+\x04(\x10\x1f\xf2\x00(\x00\x00\x00\x00\x00\x00\x00\x03\x02\x08\x86\xe0\x06\xb3B\xf8\x00\x00\x00\xb4\x00\xfa \t\x18\x18\x17R\x01\x04\x00\x01\x17\xd0+\x04(\x10\x1f\xf2\x04~'
d8=b'~\x07\x04\x00\xe1\x01\x01@\x009\x82\x00\x08\x00\x03\x01\x00H\x00\x00\x00\x00\x00L\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00p\x01\x01\x00\x00B\x01\x04\x00\x00\x00\x000\x01\x1f1\x01\x00\xe4\x02\x00d\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1e%\xa1\x00\x00H\x00\x00\x00\x00\x00L\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00p\x01\x01\x00\x017\x01\x04\x00\x00\x00\x000\x01\x1f1\x01\x04\xe4\x02\x00d\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1e%\xa1\x00\x00H\x00\x00\x00\x00\x00L\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00p\x01\x01\x00\x017\x01\x04\x00\x00\x00\x000\x01\x1f1\x01\x04\xe4\x02\x00d\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1e%\xa1\x00\xfc~'
d9=b"~\x07\x04\x00M\x01\x01@\x007\x84\x00\x06\x00\x01\x01\x00H\x00\x00\x00\x00\x00L\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00p\x01\x01\x00\x00'\x01\x04\x00\x00\x00\x000\x01\x1f1\x01\x00\xe4\x02\x00d\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1e%e\x00\xf5~"
d10=b'~\x07\x04\x00M\x01\x01@\x009\x82\x00\x12\x00\x01\x01\x00H\x00\x00\x01\x80\x00L\x00\x01\x01Y\xe6\xa8\x06\xcb\xf3\x98\x00\x00\x00\x00\x00\x00!\x02\x02\tTP\x01\x04\x00\x00\x00\x020\x01\x121\x01\x00\xe4\x02\x01\x0e\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1f\x83)\x00,~'
d11=b'~\x07\x04\x00M\x01\x01@\x009\x82\x00\x11\x00\x01\x01\x00H\x00\x00\x01\x80\x00L\x00\x01\x01Y\xe6\xa8\x06\xcb\xf3\x98\x00\x00\x00\x00\x00\x00!\x02\x02\tTP\x01\x04\x00\x00\x00\x020\x01\x121\x01\x00\xe4\x02\x01\x0e\xe5\x01\x01\xe6\x01\x00\xe7\x08\x00\x00\x00\x00\x00\x00\x00\x00\xee\n\x01\xcc\x01%=\x06\x1f\x83)\x00/~'
d12=b'~\x02\x00\x004\x010P\x03\x81\x01\x00@\x00\x00\x00\x00\x00\x00\x00\x00\x01Y\x07\xb2\x06\xcc\x17I\x00\x00\x00 \x00\xe6!\x03$\x18\x08R\x01\x04\x00\x00\x00\x00+\x04\x04\xc1\x04\xc10\x01\x151\x01\x00\x00\x04\x00\xce\x04\xc1^~'
d13=b'~\x02\x00\x004\x010P\x03\x81\x01\x00E\x00\x00\x00\x00\x00\x00\x00\x00\x01Y\x07\xb2\x06\xcc\x17I\x00\x00\x00 \x00\xe6!\x03$\x18\x10R\x01\x04\x00\x00\x00\x00+\x04\x05\xc1\x05\xc10\x01\x131\x01\x00\x00\x04\x00\xce\x05\xc1D~'
d14=b'~\x02\x00\x004\x010P\x03\x81\x01\x00M\x00\x00\x01\x00\x00\x00\x08\x00\x01Y\x07\xb2\x06\xcc\x17I\x00\x00\x00 \x00\xe6!\x03$\x18\x14\x04\x01\x04\x00\x00\x00\x00+\x04\x00n\x00n0\x01\x161\x01\x00\x00\x04\x00\xce\x00n\xb8~'
d15=b'~\x02\x00\x004\x010P\x03\x81\x01\x00V\x00\x00\x00\x00\x00\x00\x00\x00\x01Y\x07\xb2\x06\xcc\x17I\x00\x00\x00 \x00\xe6!\x03$\x18\x175\x01\x04\x00\x00\x00\x00+\x04\np\np0\x01\x161\x01\x00\x00\x04\x00\xce\np\x8c~'
d16=b'~\x02\x00\x004\x010P\x03\x81\x01\x00_\x00\x00\x00\x00\x00\x00\x00\x00\x01Y\x07\xb2\x06\xcc\x17I\x00\x00\x00 \x00\xe6!\x03$\x18!5\x01\x04\x00\x00\x00\x00+\x04\x02n\x02n0\x01\x161\x01\x00\x00\x04\x00\xce\x02n\xa5~'

if sk_res==0:
	sk.send(d2)
	result = sk.recv(1024)
	print(result)
else:
	print(sk_res)

sk.close()